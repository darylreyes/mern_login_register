const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');

const app = express();
app.use(
  cors({
    origin: 'http://localhost:3000',
    credentials: true
  })
);

app.use(cookieParser());
app.use(express.json());

mongoose.connect(
  'mongodb+srv://daryl:daryl@progpractices-icg0v.mongodb.net/todos?retryWrites=true&w=majority',
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true
  },
  () => {
    console.log('Mongoose is Connected');
  }
);

const userRouter = require('./routes/User');
app.use('/user', userRouter);

app.listen(5000, () => {
  console.log('express server started');
});
