module.exports.ErrorValidation = (username, password, role = null) => {
  const error = {};
  if (!username) error.username = 'Username is Required';
  if (!password) error.password = 'Password is Required';
  if (!role) error.role = 'Role is Required';
  return Object.keys(error).length ? error : false;
};
