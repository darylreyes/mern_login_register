import React from 'react';
import Navbar from './Component/Navbar';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from './Component/Home';
import Login from './Component/Login';
import Register from './Component/Register';
import Todos from './Component/Todos/Todos';
import Admin from './Component/Admin/Admin';
import PrivateRoute from './Component/Hoc/PrivateRoute';
import UnPrivateRoute from './Component/Hoc/UnPrivateRoute';
function App() {
  return (
    <Router>
      <Navbar />
      <Route exact path="/" component={Home} />
      <UnPrivateRoute path="/login" component={Login} />
      <UnPrivateRoute path="/register" component={Register} />
      <PrivateRoute path="/todo" roles={['user', 'admin']} component={Todos} />
      <PrivateRoute path="/admin" roles={['admin']} component={Admin} />
    </Router>
  );
}

export default App;
