import React, { useContext, useState } from 'react';
import PropTypes from 'prop-types';
import AuthService from '../Services/AuthService';
import { AuthContext } from '../Context/AuthContext';
import Message from './Message';

function Login(props) {
  const [userData, setUser] = useState({ username: '', password: '' });
  const [error, setError] = useState({});
  const [message, setMessage] = useState(null);
  const authContext = useContext(AuthContext);
  const handleChange = e => {
    setUser({ ...userData, [e.target.name]: e.target.value });
  };
  const handleSubmit = async e => {
    e.preventDefault();
    try {
      const { isAuthenticated, user, message } = await AuthService.login(
        userData
      );
      if (isAuthenticated) {
        authContext.setUser(user);
        authContext.setIsAuthenticated(isAuthenticated);
        props.history.push('/todo');
      } else {
        message && setError(message.error);
      }
      setMessage(message);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div>
      <div>
        <form onSubmit={handleSubmit}>
          <h3>Please Sign In</h3>
          <label className="form-label">Username</label>
          <div className="input-group has-validation">
            <input
              value={userData.username}
              type="text"
              name="username"
              onChange={handleChange}
              className={`form-control ${
                error['username'] ? 'is-invalid' : ''
              } `}
              placeholder="Enter username"
            />
            <div className="invalid-feedback">Username is Required</div>
          </div>
          <label htmlFor="password" className="sr-only">
            Password:
          </label>
          <div className="input-group has-validation">
            <input
              value={userData.password}
              type="password"
              name="password"
              onChange={handleChange}
              className={`form-control ${
                error['password'] ? 'is-invalid' : ''
              } `}
              placeholder="Enter Password"
            />
            <div className="invalid-feedback">Password is Required</div>
          </div>
          <button className="btn btn-lg btn-primary btn-block" type="submit">
            Login
          </button>
        </form>
        {message ? <Message message={message} /> : null}
      </div>
    </div>
  );
}

Login.propTypes = {};

export default Login;
