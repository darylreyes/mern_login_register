import React, { useContext, useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import AuthService from '../Services/AuthService';

import Message from './Message';

function Register(props) {
  const [userData, setUserData] = useState({
    username: '',
    password: '',
    role: ''
  });
  const [error, setError] = useState({});
  const [message, setMessage] = useState(null);
  let timerId = useRef(null);

  useEffect(() => {
    return () => {
      clearTimeout(timerId);
    };
  }, []);
  const handleChange = e => {
    setUserData({ ...userData, [e.target.name]: e.target.value });
    setError({ ...error, [e.target.name]: null });
  };

  const resetForm = () => {
    setUserData({ username: '', password: '', role: '' });
  };

  const handleSubmit = async e => {
    try {
      e.preventDefault();
      const { message } = await AuthService.register(userData);
      if (!message.msgError) {
        timerId = setTimeout(() => {
          props.history.push('/login');
        }, 2000);
      } else {
        message.error && setError(message.error);
      }
      setMessage(message);
      resetForm();
    } catch (error) {
      console.log(error);
    }
  };
  console.log(error);
  return (
    <div>
      <div>
        <form onSubmit={handleSubmit}>
          <h3>Register</h3>
          <label className="form-label">Username</label>
          <div className="input-group has-validation">
            <input
              value={userData.username}
              type="text"
              name="username"
              onChange={handleChange}
              className={`form-control ${
                error['username'] ? 'is-invalid' : ''
              } `}
              placeholder="Enter username"
            />
            <div className="invalid-feedback">Username is Required</div>
          </div>
          <label htmlFor="password" className="sr-only">
            Password:
          </label>
          <div className="input-group has-validation">
            <input
              value={userData.password}
              type="password"
              name="password"
              onChange={handleChange}
              className={`form-control ${
                error['password'] ? 'is-invalid' : ''
              } `}
              placeholder="Enter Password"
            />
            <div className="invalid-feedback">Password is Required</div>
          </div>
          <label htmlFor="role" className="form-label">
            Role
          </label>
          <div className="input-group has-validation">
            <select
              className={`form-select ${error['role'] ? 'is-invalid' : ''} `}
              name="role"
              value={userData.role}
              onChange={handleChange}
              aria-describedby="validationServer04Feedback"
            >
              <option value="">Choose...</option>
              <option value="admin">Admin</option>
              <option value="user">User</option>
            </select>
            <div className="invalid-feedback">Role is Required</div>
          </div>
          <button
            className="btn btn-lg btn-primary btn-block m-2"
            type="submit"
          >
            Register
          </button>
        </form>
        {message ? <Message message={message} /> : null}
      </div>
    </div>
  );
}

Register.propTypes = {};

export default Register;
