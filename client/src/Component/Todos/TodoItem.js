import React from 'react';
import PropTypes from 'prop-types';

function TodoItem({ todo }) {
  return <li>{todo.name}</li>;
}

TodoItem.propTypes = {};

export default TodoItem;
