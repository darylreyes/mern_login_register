import axiosCaller from '../utils/axiosCaller';

export default {
  getTodos: async () => {
    try {
      let data = {};
      const res = await fetch('user/todos');
      if (res.status === 401) {
        return { message: { msgBody: 'UnAuthorized' }, msgError: true };
      }
      data = await res.json();
      return data;
    } catch (error) {
      console.log(error);
      return error;
    }
  },
  postTodo: async todo => {
    const data = await fetch('user/todo', {
      method: 'post',
      body: JSON.stringify(todo),
      headers: {
        'Content-Type': 'application/json'
      }
    });
    if (data.status === 401)
      return { message: { msgBody: 'UnAuthorized' }, msgError: true };
    // const data = await axiosCaller('POST', todo, 'user/todo');
    return await data.json();
  }
};
