import PropTypes from 'prop-types';
import axios from 'axios';

const axiosCaller = async (method, data, url) => {
  let result;
  const axiosData = {
    method,
    ...(data && { data }),
    withCredentials: true,
    url: `http://localhost:5000/${url}`
  };
  try {
    result = await axios(axiosData);
    return result.data;
  } catch (error) {
    return error.response.data;
  }
};

axiosCaller.propTypes = {
  method: PropTypes.string,
  data: PropTypes.object,
  url: PropTypes.string
};

export default axiosCaller;
